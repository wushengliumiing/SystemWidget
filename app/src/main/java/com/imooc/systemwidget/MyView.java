package com.imooc.systemwidget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class MyView extends View {

    private int width;
    private int height;
    private int length;

    public MyView(Context context, AttributeSet attrs,
                  int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        if(width>height){
            length = height;
        }else{
            length = width;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 绘制圆

        Paint paint = new Paint();
        paint.setColor(Color.RED);                          //设置画笔色彩
        paint.setStyle(Style.STROKE);                       //设置画笔为空心
        paint.setStrokeWidth((float) 10.0);             //设置线宽
        canvas.drawColor(Color.WHITE);
        /*canvas.drawLine(50, 50, 450, 50, paint);            //绘制直线
        canvas.drawRect(100, 100, 200, 600, paint);         //绘制矩形
        canvas.drawRect(300, 100, 400, 600, paint);         //绘制矩形*/
        RectF rectF = new RectF();
        rectF.top = 0;
        rectF.left = 0;
        rectF.right = length;
        rectF.bottom = length;
        canvas.drawArc(rectF,0,360,false,paint);




    }




}
